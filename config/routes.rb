Rails.application.routes.draw do

  namespace :api, defaults: { format: :json}  do
    get 'events/important'
    get 'events/all'
    get 'bookings/booking_total'
    get 'employees', to: 'employees#index'
  end

  resources :bookings
  resources :children
  resources :booking_service_lines
  resources :customer_bookings
  get 'booking_employees/:booking_id/multiadd', to: 'booking_employees#index', as: 'booking_employees_multiadd'
  post 'booking_employees/save', to: 'booking_employees#save', as: 'booking_employees_save'
  post 'booking_employees/add', to: 'booking_employees#new', defaults: { format: :js }
  post 'booking_employees/:booking_id/multiadd/save', to: 'booking_employees#save'
  resources :booking_employees
  get 'employees/child_parent_data_set', to: 'employees#child_parent_data_set'
  resources :employees
  resources :employee_types
  resources :discount_types
  resources :customers do
    collection { post :import }
  end
  resources :child_parents
  resources :service_fees
  get 'booking_employees/multiadd', to: 'booking_employees#multiadd'
  get 'general_settings/index'
  post 'customers/get_latest_bookings', to: 'customers#get_latest_bookings', defaults: {format: :json}

  devise_for :users
  get 'index/Index'

  # lol
  get 'supersecret', to: 'index#Julius'
  get 'burntbanana', to: redirect("https://youtu.be/c7tOAGY59uQ?t=18s")


  root 'index#Index'

  # lookup customers in booking form
  post 'customers/lookup', to: 'customers#lookup', defaults: {format: :json}

  # adding services to booking form
  post 'bookings/:id/add_service', to: 'bookings#add_service', defaults: {format: :js}

  # reports
  get 'reports/customer', to: 'reports#customers_by_bookings'
  get 'reports/customer.json', to: "reports#customers_by_bookings", defaults: {format: :json}, as: 'reports_customers_by_bookings'
  get 'reports/month_birthdays', to: 'reports#month_birthdays'
  get 'reports/month_birthdays.json', to: "reports#month_birthdays", defaults: {format: :json}, as: 'reports_month_birthdays_json'
  get 'reports/popular_services', to: 'reports#popular_services'
  post 'reports/popular_services.json', to: 'reports#popular_services', defaults: {format: :json}, as: 'reports_popular_services_json'
  get 'reports/bookings', to: 'reports#bookings'
  post 'reports/bookings.json', to: 'reports#bookings', defaults: {format: :json}, as: 'reports_bookings_json'
  get 'reports/revenue_by_service', to: 'reports#revenue_by_service'
  post 'reports/revenue_by_service.json', to: 'reports#revenue_by_service', defaults: {format: :json}, as: 'reports_revenue_by_service_json'
  get 'reports/booking_per_dt', to: 'reports#booking_per_dt'
  post 'reports/bookings_per_dt.json', to: 'reports#booking_per_dt', defaults: {format: :json}, as: 'reports_bookings_per_dt_json'
  get 'dashboard', to: 'dashboard#index'

  #mailer routes
  get '/reminder/all', to: 'bookings#send_monthly_reminder'
  get '/reminder/bday', to: 'customers#send_bday_reminder'
  get '/reminder/:booking_id', to: 'bookings#send_reminder'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #change service_fees.json
  get 'as_json.json', to: 'service_fees#as_json', defaults: {format: :json}
end
