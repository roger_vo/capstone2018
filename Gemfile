source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
# Use for heroku deployment
gem 'dotenv-rails', groups: [:development, :test]
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes

gem 'pg', '~> 1.1', '>= 1.1.3'

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
#gem 'bcrypt', '~> 3.1.7'

gem 'bcrypt-ruby', '3.1.0', :require => 'bcrypt'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'fullcalendar-rails'
gem 'momentjs-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'bootstrap'
gem 'devise'
gem 'chronic'
gem 'cocoon'
gem 'chartkick'
gem 'rails-assets-jquery', source: 'https://rails-assets.org'
gem 'rails-assets-datatables', source: 'https://rails-assets.org'
gem 'kaminari'
gem 'bootstrap-daterangepicker-rails'
gem 'toastr-rails'
gem 'bootstrap-toggle-rails'
gem 'baby_squeel'
gem 'groupdate'
gem 'paranoia'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  gem "capistrano","~> 3.10", require: false
  gem "capistrano-rails", "~> 1.3", require: false 
  gem "capistrano-passenger"
  gem "capistrano-rvm"
  gem "sqlite3", platform: :ruby
  gem "pry-rails"
end

group :staging, :development do 
  gem "faker"
end

group :development do
  gem 'pry'
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :Docker do
  gem 'tzinfo-data'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'wdm', '>= 0.1.0' if Gem.win_platform?

