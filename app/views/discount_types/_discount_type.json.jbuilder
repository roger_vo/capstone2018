json.extract! discount_type, :id, :discount_type, :deleted_at, :created_at, :updated_at
json.url discount_type_url(discount_type, format: :json)
