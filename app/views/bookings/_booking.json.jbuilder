json.extract! booking, :id, :discount_type_id, :start, :end, :bookingdetails, :discountamount, :bookingtotal, :deleted_at, :created_at, :updated_at
json.url booking_url(booking, format: :json)
