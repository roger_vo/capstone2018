json.extract! employee_type, :id, :employee_type, :deleted_at, :created_at, :updated_at
json.url employee_type_url(employee_type, format: :json)
