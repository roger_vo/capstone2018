json.extract! booking_employee, :id, :employee_id, :booking_id, :be_start_time, :be_end_time, :deleted_at, :created_at, :updated_at
json.url booking_employee_url(booking_employee, format: :json)
