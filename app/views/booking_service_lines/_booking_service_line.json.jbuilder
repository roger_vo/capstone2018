json.extract! booking_service_line, :id, :booking_id, :service_fee_id, :deleted_at, :created_at, :updated_at
json.url booking_service_line_url(booking_service_line, format: :json)
