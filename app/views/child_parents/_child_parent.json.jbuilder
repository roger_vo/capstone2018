json.extract! child_parent, :id, :child_id, :customer_id, :deleted_at, :created_at, :updated_at
json.url child_parent_url(child_parent, format: :json)
