json.extract! employee, :id, :employee_type_id, :ef_name, :el_name, :deleted_at, :created_at, :updated_at
json.url employee_url(employee, format: :json)
