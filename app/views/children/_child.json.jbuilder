json.extract! child, :id, :c_fName, :c_lName, :c_birthday, :deleted_at, :created_at, :updated_at
json.url child_url(child, format: :json)
