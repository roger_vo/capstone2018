json.extract! event, :id, :title, :start, :end, :description
json.edit_callback edit_booking_path(event.id)
