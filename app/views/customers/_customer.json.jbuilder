json.extract! customer, :id, :cf_name, :cl_name, :address1, :address2, :city, :state_id, :zipcode, :birthday, :phone1, :phone2, :email,  :state_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
