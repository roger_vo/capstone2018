json.extract! customer_booking, :id, :booking_id, :customer_id, :deleted_at, :created_at, :updated_at
json.url customer_booking_url(customer_booking, format: :json)
