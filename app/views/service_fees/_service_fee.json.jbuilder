json.extract! service_fee, :id ,:std_price, :service_desc, :deleted_at, :created_at, :updated_at
json.url service_fee_url(service_fee, format: :json)
