class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  def reminder(user_id)
    @user = Customer.find(user_id) 
    if @user 
      mail(to: @user.email, subject: 'TerriG Reminder')
    end
  end

  def bday_reminder(child_id)
    @child = Child.find(child_id)
    @user = @child.customers.first

    if @child and @user
      mail(to: @user.email, subject: "#{@child} has a birthday coming up!")
    end

  end
end
