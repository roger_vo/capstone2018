class State < ApplicationRecord
  has_many :customers

  acts_as_paranoid
end
