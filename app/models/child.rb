class Child < ApplicationRecord
  has_many :child_parents
  has_many :customers, through: :child_parents

  acts_as_paranoid

  def start
    unless c_birthday.blank?
      Date.new(DateTime.now.year, c_birthday.month, c_birthday.day)
    end
  end

  def end
    unless c_birthday.blank?
      Date.new(DateTime.now.year, c_birthday.month, c_birthday.day)
    end
  end

  def title
    to_s
  end

  def age(as_at = Time.now)
    as_at = as_at.utc.to_date if as_at.respond_to?(:utc)
    as_at.year - c_birthday.year - ((as_at.month > c_birthday.month || (as_at.month == c_birthday.month && as_at.day >= c_birthday.day)) ? 0 : 1)
  end

  def to_s
    "#{c_fName} #{c_lName}"
  end

end
