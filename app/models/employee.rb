class Employee < ApplicationRecord
  belongs_to :employee_type
  has_many :booking_employees
  has_many :bookings, through: :booking_employees

  acts_as_paranoid
end
