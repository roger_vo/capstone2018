class BookingServiceLine < ApplicationRecord
  belongs_to :booking
  belongs_to :service_fee

  accepts_nested_attributes_for :service_fee, reject_if: :all_blank 
  validates_associated :service_fee

  acts_as_paranoid
  # scopes
  scope :by_year, -> (year) { 
    joins(:booking).where('bookings.start_date BETWEEN ? AND ?',Date.new(year), Date.new(year).end_of_year) 
  }

  scope :by_quarter, -> (year, quarter) {
    joins(:booking).where('bookings.start_date BETWEEN ? AND ?', Date.new(year, ((quarter * 3) - 2)), Date.new(year, (quarter * 3)).end_of_month)
  }

  scope :by_month, -> (year, month) {
    joins(:booking).where('bookings.start_date BETWEEN ? AND ?', Date.new(year, month), Date.new(year, month).end_of_month)
  }

  def date_of_booking(fmt = "%Y-%m-%d")
    booking.start_date.strftime(fmt)
  end


end
