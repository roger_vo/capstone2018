class BookingEmployee < ApplicationRecord
  belongs_to :employee
  belongs_to :booking

  acts_as_paranoid
end
