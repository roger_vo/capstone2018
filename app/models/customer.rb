class Customer < ApplicationRecord
  belongs_to :state, required: false
  has_many :child_parents, inverse_of: :customer
  has_many :children, through: :child_parents

  acts_as_paranoid


  has_many :customer_bookings, inverse_of: :customer
  has_many :bookings, through: :customer_bookings, inverse_of: :customers
  accepts_nested_attributes_for :child_parents, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :children, reject_if: :all_blank, allow_destroy: true
  validates :cf_name, :cl_name, :phone1, :email, presence: { message: "Customer fields cannot be blank" }

  def to_s
    "#{cf_name} #{cl_name}"
  end

  def title
    to_s
  end

  def start
    Date.new(DateTime.now.year, birthday.month, birthday.day)
  end

  def end
    Date.new(DateTime.now.year, birthday.month, birthday.day)
  end

  def age(as_at = Time.now)
    as_at = as_at.utc.to_date if as_at.respond_to?(:utc)
    as_at.year - birthday.year - ((as_at.month > birthday.month || (as_at.month == birthday.month && as_at.day >= birthday.day)) ? 0 : 1)
  end

  def next_booking
    startdate = DateTime.now.to_date
    enddate = 6.months.from_now.to_date
    upcoming_bookings = bookings.where(start_date: startdate..enddate)
    nextbooking = nil
    upcoming_bookings.each do |booking|
      if !nextbooking || ((booking.start_date - startdate) > (nextbooking.start_date - start_date))
        nextbooking = booking
      end
    end
    nextbooking
  end
  # James' work
  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Customer.create! row.to_hash
    end
  end

  def total_bookings
    self.bookings.count
  end

end
