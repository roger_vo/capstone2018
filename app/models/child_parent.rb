class ChildParent < ApplicationRecord
  belongs_to :child
  belongs_to :customer
  accepts_nested_attributes_for :child, reject_if: :all_blank

  acts_as_paranoid
end
