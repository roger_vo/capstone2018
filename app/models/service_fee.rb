class ServiceFee < ApplicationRecord
  has_many :booking_service_lines, inverse_of: :service_fee
  has_many :bookings, through: :booking_service_lines, inverse_of: :service_fees

  acts_as_paranoid

  validates :std_price, :service_desc, presence: true
  
  def to_s
    service_desc
  end
end
