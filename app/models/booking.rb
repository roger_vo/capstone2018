class Booking < ApplicationRecord
  acts_as_paranoid

  belongs_to :discount_type, required: false

  has_many :customer_bookings, inverse_of: :booking, dependent: :destroy
  has_many :customers, through: :customer_bookings, inverse_of: :bookings, index_errors: true 


  has_many :booking_service_lines, inverse_of: :booking, dependent: :destroy
  has_many :service_fees, through: :booking_service_lines, inverse_of: :bookings, dependent: :destroy

  has_many :booking_employees, inverse_of: :booking, dependent: :destroy
  has_many :employees, through: :booking_employees, inverse_of: :bookings, dependent: :destroy


  accepts_nested_attributes_for :booking_service_lines, reject_if: :all_blank, allow_destroy: true

  #
  # Customer validation
  #
  #validate :validate_single_customer
  validates_associated :customers

  #
  # Booking validation
  #
  validates :start_date, :start_time, :end_date, :end_time, presence: true
  validates_associated :booking_service_lines

  # Discount / total can only be numbers
  validates :discountamount, :bookingtotal, format: { with: /\A(\d*\.)?\d+\z/, message: "Discount/Total should be in 00.00 format!" }, allow_blank: true
  validates :discountamount, numericality: { less_than_or_equal_to: :bookingtotal }, allow_blank: true

  validate :validate_date_within_six_months
  validate :validate_double_booking

  
  # scopes
  scope :by_year, -> (year) { 
    where(start_date: Date.new(year)..Date.new(year).end_of_year) 
  }

  scope :by_quarter, -> (year, quarter) {
    where(start_date: Date.new(year, ((quarter * 3) - 2))..Date.new(year, (quarter * 3)).end_of_month)
  }

  scope :by_month, -> (year, month) {
    where(start_date: Date.new(year, month)..Date.new(year, month).end_of_month)
  }

  acts_as_paranoid 


  def title
    "#{customers.first}" 
  end

  def total
    bookingtotal - (discountamount || 0)
  end

  def description
    desc = "Time:<br> #{start_time_string} - #{end_time_string}<br>"
    desc = desc +  "Services:<br>"
    service_fees.each do |s|
      desc = desc + " -#{s.service_desc}<br>"
    end
    desc = desc + "Total:<br>#{"$%.2f" % total}"
    desc
  end

   def start
    concat_time(start_date, local_time(start_time))
   end

  def end
    concat_time(end_date, local_time(end_time))
  end
  def start=(value)
    time = Chronic.parse(value)
    self.start_date = time.to_date
    self.start_time = time.to_time
  end
  def end=(value)
    time = Chronic.parse(value)
    self.end_date = time.to_date
    self.end_time = time.to_time
  end

  def start_time_string
    if start_time
      local_time(self.start_time).strftime("%I:%M%p")
    end
  end

  def start_time_string=(value)
    if value
      self.start_time = Chronic.parse(value)
    end
  end

  def end_time_string 
    if end_time
      local_time(self.end_time).strftime("%I:%M%p")
    end
  end

  def end_time_string=(value)
    if value
      self.end_time = Chronic.parse(value)
    end
  end

  def concat_time(date, time)
    Time.new(date.year, date.month, date.day, time.hour, time.min)
  end

  def local_time(utc_time)
    tz = TZInfo::Timezone.get("America/Chicago")
    tz.utc_to_local(utc_time)
  end

  def validate_single_customer
      self.errors.add :base, "Booking must have 1 customer!" unless customers.first.valid?
  end

  def conflicts?
		return  Booking.where('(bookings.id != ?) AND (bookings.start_date = ?) AND ((bookings.start_time BETWEEN ? AND ?) OR (bookings.end_time BETWEEN ? AND ?))', self.id, self.start_date, self.start_time, self.end_time, self.start_time, self.end_time).count > 0
  end



  def validate_date_within_six_months
    self.errors.add :base, "Date cannot be more than 6 months in advance!" unless  start_date.past? || (Date.today..6.month.from_now).include?(start_date)
  end

  def validate_double_booking
    self.errors.add :base, "You already have a booking at this time!" if self.conflicts? 
  end


end
