class EmployeeType < ApplicationRecord

  acts_as_paranoid

  def to_s
    employee_type
  end
end
