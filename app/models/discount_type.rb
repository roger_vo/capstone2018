class DiscountType < ApplicationRecord
  has_many :bookings
  has_many :booking_service_lines

  acts_as_paranoid

  def to_s
    discount_type
  end
end
