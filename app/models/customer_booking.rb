class CustomerBooking < ApplicationRecord
  belongs_to :booking
  belongs_to :customer

  #validates_presence_of :booking
  accepts_nested_attributes_for :customer, reject_if: :all_blank 

  acts_as_paranoid
end
