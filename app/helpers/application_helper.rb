module ApplicationHelper  
  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def sanitize_phone(input)
    input_mask =  /\A\((\d{3})\)\ (\d{3})-(\d{4})\z/   
    output_mask = /\A(?:\d{10})\z/

    if (match = input_mask.match(input)) && match.size == 4
      input = "#{match[1]}#{match[2]}#{match[3]}"
    end
    if output_mask.match(input)
      return input
    end
    nil
  end

  def greet
    now = Time.now
    today = Date.today.to_time

    morning = today.beginning_of_day
    noon = today.noon
    evening = today.change( hour: 17 )
    night = today.change( hour: 20 )
    tomorrow = today.tomorrow

    if (morning..noon).cover? now
      'Good Morning'
    elsif (noon..evening).cover? now
      'Good Afternoon'
    elsif (evening..night).cover? now
      'Good Evening'
    elsif (night..tomorrow).cover? now
      'Good Night'
    end
  end
end
