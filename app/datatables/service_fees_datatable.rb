class ServiceFeesDatatable < ApplicationDatatable
  delegate :edit_service_fee_path, to: :@view
  delegate :service_fee_path, to: :@view

  private

  def data
    service_fees.map do |service_fee|
      [].tap do |column|
        column << service_fee.id
        column << service_fee.service_desc
        column << "$%.2f" % service_fee.std_price
        column << link_to('Edit', edit_service_fee_path(service_fee), class: 'btn btn-info btn-sm') + link_to('Delete', service_fee_path(service_fee), method: :delete,data: { confirm: 'Are you sure?' },  class: 'ml-2 btn btn-danger btn-sm')

      end
    end
  end

  def count
    ServiceFee.count
  end

  def total_entries
    service_fees.total_count
  end

  def service_fees
    @service_fees ||= fetch_service_fees
  end

  def fetch_service_fees
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end
    service_fees = ServiceFee.order("#{sort_column} #{sort_direction}")
    service_fees = service_fees.page(page).per(per_page)
    service_fees = service_fees.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(id service_desc std_price deleted_at created_at updated_at)
  end
end
