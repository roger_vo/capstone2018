class BookingsDatatable < ApplicationDatatable
  delegate :edit_booking_path, to: :@view
  delegate :booking_path, to: :@view

  private
  def data
    bookings.map do |bk|
      [].tap do |column|
        column << bk.bookingdetails
        column << DiscountType.where(id: bk.discount_type_id).pluck(:discount_type)
        column << '$'+(bk.discountamount.nil? ? "N/a" : ('%.2f' % bk.discountamount))
        column << '$'+(bk.bookingtotal.nil? ? "N/a" : ('%.2f' % bk.bookingtotal))
        column << bk.start_date
        column << bk.start_time.in_time_zone("Central Time (US & Canada)").strftime("%I:%M%p")
        column << bk.end_date
        column << bk.end_time.in_time_zone("Central Time (US & Canada)").strftime("%I:%M%p")
        column << link_to('Edit', edit_booking_path(bk), class: 'btn btn-info btn-sm') + link_to('Delete', booking_path(bk), method: :delete, data: { confirm: 'Are you sure?' }, class: 'ml-2 btn btn-danger btn-sm')
      end
    end
  end

  def count
    Booking.count
  end

  def total_entries
    bookings.total_count
  end

  def bookings
    @customer_bookings ||= fetch_bookings
  end

  def fetch_bookings
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end

    # will_paginate
    # users = User.page(page).per_page(per_page)
    bookings = Booking.order("#{sort_column} #{sort_direction}")
    bookings = bookings.page(page).per(per_page)
    bookings = bookings.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(id discount_type_id bookingdetails discountamount bookingtotal updated_at start_date start_time end_date end_time)
  end
end
