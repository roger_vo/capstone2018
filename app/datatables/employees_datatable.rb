class EmployeesDataTable < ApplicationDatatable
  delegate :edit_employee_path, to: :@view
  delegate :employee_path, to: :@view

  private
  def data
    employees.map do |employee|
      [].tap do |column|
        column << employee.id
        column << employee.ef_name
        column << employee.el_name
        column << link_to('Edit', edit_employee_path(discount_type), class: 'btn btn-info btn-sm') + link_to('Delete', employee_path(discount_type), method: :delete,data: { confirm: 'Are you sure?' }, class: 'ml-2 btn btn-danger btn-sm')

      end
    end
  end

  def count
    Employee.count
  end

  def total_entries
    employees.total_count
  end

  def employees 
    @employees ||= fetch_employees
  end

  def fetch_employees
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end

    employees = Employee.order("#{sort_column} #{sort_direction}")
    employees = employees.page(page).per(per_page)
    employees = employees.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(id ef_name el_name)
  end
end
