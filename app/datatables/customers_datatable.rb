class CustomersDatatable < ApplicationDatatable
  delegate :edit_customer_path, to: :@view
  delegate :customer_path, to: :@view

  private
  def data
    customers.map do |customer|
      [].tap do |column|
        column << customer.id
        column << customer.cf_name + ' ' + customer.cl_name
        column << customer.address1
        column << customer.city
        column << State.where(id: customer.state_id).pluck(:state_abbvr)
        column << customer.zipcode
        column << customer.birthday
        column << customer.phone1
        column << customer.email
        column << customer.facebook_handle
        column << customer.instagram_handle
        column << customer.twitter_handle
        column << customer.customer_type
        column << link_to('Edit', edit_customer_path(customer), class: 'btn btn-info btn-sm') + link_to('Delete', customer_path(customer), method: :delete,data: { confirm: 'Are you sure?' }, class: 'ml-1 btn btn-danger btn-sm')
      end
    end
  end

  def count
    Customer.count
  end

  def total_entries
    customers.total_count
  end

  def customers
    @customers ||= fetch_customers
  end

  def fetch_customers
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end

    # will_paginate
    # users = User.page(page).per_page(per_page)
    customers = Customer.order("#{sort_column} #{sort_direction}")
    customers = customers.page(page).per(per_page)
    customers = customers.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(id cf_name cl_name address1 address2 city state_id zipcode birthday phone1 phone2 email created_at updated_at facebook_handle instagram_handle twitter_handle customer_type)
  end
end
