class ChildrenDatatable < ApplicationDatatable
  delegate :edit_child_path, to: :@view
  delegate :child_path, to: :@view

  private
  def data
    children.map do |child|
      [].tap do |column|
        column << child.id
        column << child.c_fName
        column << child.c_lName
        column << child.c_birthday
        column << child.deleted_at
        column << link_to('Edit', edit_child_path(child), class: 'btn btn-info btn-sm') + link_to('Delete', child_path(child), method: :delete,data: { confirm: 'Are you sure?' }, class: 'ml-2 btn btn-danger btn-sm')

      end
    end
  end

  def count
    Child.count
  end

  def total_entries
    children.total_count
  end

  def children
    @children ||= fetch_children
  end

  def fetch_children
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end

    children = Child.order("#{sort_column} #{sort_direction}")
    children = children.page(page).per(per_page)
    children = children.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(id "c_fName" "c_lName" c_birthday deleted_at created_at updated_at)
  end
end
