class DiscountTypesDatatable < ApplicationDatatable
  delegate :edit_discount_type_path, to: :@view
  delegate :discount_type_path, to: :@view

  private

  def data
    discount_types.map do |discount_type|
      [].tap do |column|
        column << discount_type.id
        column << discount_type.discount_type
        column << link_to('Edit', edit_discount_type_path(discount_type), class: 'btn btn-info btn-sm') + link_to('Delete', discount_type_path(discount_type), method: :delete, data: { confirm: 'Are you sure?' }, class: 'ml-2 btn btn-danger btn-sm')

      end
    end
  end

  def count
    DiscountType.count
  end

  def total_entries
    discount_types.total_count
  end

  def discount_types
    @discount_types ||= fetch_discount_types
  end

  def fetch_discount_types
    search_string = []
    columns.each do |term|
      search_string << "cast(#{term} as text) ~* :search"
    end

    # will_paginate
    # users = User.page(page).per_page(per_page)
    discount_types = DiscountType.order("#{sort_column} #{sort_direction}")
    discount_types = discount_types.page(page).per(per_page)
    discount_types = discount_types.where(search_string.join(' or '), search: ".*#{params[:search][:value]}.*")
  end

  def columns
    %w(discount_type deleted_at created_at updated_at)
  end
end
