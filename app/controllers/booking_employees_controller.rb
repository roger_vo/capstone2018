class BookingEmployeesController < ApplicationController
  before_action :set_booking_employee, only: [:show, :edit, :update, :destroy]

  # GET /booking_employees
  # GET /booking_employees.json
  def index
    @booking_employees = BookingEmployee.all
    if params.has_key?(:booking_id)
      @booking = Booking.find(params[:booking_id])
      render :index
    end
  end

  # GET /booking_employees/1
  # GET /booking_employees/1.json
  def show
  end

  # POST /booking_employees/new
  def new
    @booking_employee = BookingEmployee.new
    if (employee_id = params[:employee_id]) && (booking_id = params[:booking_id])
      employee = Employee.find(employee_id)  
      booking = Booking.find(booking_id)
      unless booking.employees.exists?(employee_id)

      @booking_employee.employee = employee
      @booking_employee.booking = booking
      @booking_employee.save

      respond_to do |format|
        format.js { render partial: 'new', locals: { be: @booking_employee} }
      end
      else
        head 304
      end
    else 
      head 404 
    end

  end

  # GET /booking_employees/1/edit
  def edit
  end

  # POST /booking_employees
  # POST /booking_employees.json
  def create
    @booking_employee = BookingEmployee.new(booking_employee_params)

    respond_to do |format|
      if @booking_employee.save
        format.html { redirect_to @booking_employee, notice: 'Booking employee was successfully created.' }
        format.json { render :show, status: :created, location: @booking_employee }
      else
        format.html { render :new }
        format.json { render json: @booking_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /booking_employees/1
  # PATCH/PUT /booking_employees/1.json
  def update
    respond_to do |format|
      if @booking_employee.update(booking_employee_params)
        format.html { redirect_to @booking_employee, notice: 'Booking employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking_employee }
      else
        format.html { render :edit }
        format.json { render json: @booking_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /booking_employees/1
  # DELETE /booking_employees/1.json
  def destroy
    @booking_employee.destroy
    respond_to do |format|
      format.js { render partial: 'delete', locals: { be: @booking_employee },  notice: 'Booking employee was successfully deleted.' }
      # format.json { head :no_content }
    end
  end

  # Our huge multi-adder for employeebookings
  def multiadd
  end 

  # POST /booking_employees/add
  def add
    if id = params[:id]
      employee = Employee.find(id)  

      render partial: 'booking_employees/add.js.erb', locals: { employee: employee }
    end
  end


  # POST /booking_employees/save
  def save
    if booking_employee_params.has_key?(:save_data)
      hash = booking_employee_params[:save_data].to_h
      hash.each do |k, v|
        if be = BookingEmployee.find(v[:id])
          be_start = Chronic.parse(v[:be_start_time])
          be_end = Chronic.parse(v[:be_end_time])
          be.update(be_start_time: be_start, be_end_time: be_end)
        end
      end
      render js: "toastr['success']('saved');" 
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_booking_employee
    @booking_employee = BookingEmployee.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def booking_employee_params
    params.require(:booking_employee).permit(:id, :employee_id, :booking_id, :be_start_time, :be_end_time, :deleted_at, save_data: {})
  end
end
