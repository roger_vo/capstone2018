class Api::BookingsController < ApplicationController
  def booking_total
    unless params[:id].nil?
      begin
        render json: {total: Booking.find(params[:id]).bookingtotal}
      rescue ActiveRecord::RecordNotFound
        render json: 0
      end
    end
  end
end
