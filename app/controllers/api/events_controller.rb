class Api::EventsController < ApplicationController
  def all
    @events = Booking.all
    render :events
  end

  def important
    @birthdays = Child.all.where.not(c_birthday: nil)
    render :birthdays
  end
end
