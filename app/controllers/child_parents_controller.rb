class ChildParentsController < ApplicationController
  before_action :set_child_parent, only: [:show, :edit, :update, :destroy]

  # GET /child_parents
  # GET /child_parents.json
  def index
    @child_parents = ChildParent.all
  end

  # GET /child_parents/1
  # GET /child_parents/1.json
  def show
  end

  # GET /child_parents/new
  def new
    @child_parent = ChildParent.new
  end

  # GET /child_parents/1/edit
  def edit
  end

  # POST /child_parents
  # POST /child_parents.json
  def create
    @child_parent = ChildParent.new(child_parent_params)

    respond_to do |format|
      if @child_parent.save
        format.html { redirect_to @child_parent, notice: 'Child parent was successfully created.' }
        format.json { render :show, status: :created, location: @child_parent }
      else
        format.html { render :new }
        format.json { render json: @child_parent.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /child_parents/1
  # PATCH/PUT /child_parents/1.json
  def update
    respond_to do |format|
      if @child_parent.update(child_parent_params)
        format.html { redirect_to @child_parent, notice: 'Child parent was successfully updated.' }
        format.json { render :show, status: :ok, location: @child_parent }
      else
        format.html { render :edit }
        format.json { render json: @child_parent.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /child_parents/1
  # DELETE /child_parents/1.json
  def destroy
    @child_parent.destroy
    respond_to do |format|
      format.html { redirect_to child_parents_url, notice: 'Child parent was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_child_parent
      @child_parent = ChildParent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def child_parent_params
      params.require(:child_parent).permit(:child_id, :customer_id, :deleted_at)
    end
end
