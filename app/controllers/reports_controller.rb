class ReportsController < ApplicationController

  def booking_per_dt
    hash = Booking.where("extract(year from start_date) = ?", params[:year]).group(:discount_type_id).count
    new_hash = hash.collect{|k, v| [DiscountType.where(id: k).pluck(:discount_type), v]}

    respond_to do |format|
      format.html { render }
      format.json { render json: new_hash }
    end
  end


  def customers_by_bookings
    customers = Customer.joins(:customer_bookings).group(:id).order("COUNT('customers.id') DESC").first(5)
    @hash = Hash.new
    customers.map { |customer|
      increment_hash_by(@hash, customer, customer.bookings.count)
    }

    respond_to do |format|
      format.html { render }
      format.json { render json: @hash}
    end
  end

  def month_birthdays
    children = Child.all
    hash = Hash.new
    children.map { |child|
      unless child.c_birthday.nil? then
        month = child.c_birthday.strftime("%B")
        unless hash[month].nil?
          hash[month] += 1
        else
          hash[month] = 1
        end
      end
    }
    respond_to do |format|
      format.html { render }
      format.json {render json: hash}
    end
  end

  def popular_services
    services = nil
    @hash = Hash.new

    if params.has_key?(:year)
      if params.has_key?(:month)
        services = BookingServiceLine.select('service_fees.service_desc').by_month(params[:year].to_i, Date::MONTHNAMES.index(params[:month])).joins(:service_fee).group('service_fees.service_desc').order('COUNT(service_fee_id) DESC').count.first(8)
      elsif params.has_key?(:quarter)
        services = BookingServiceLine.select('service_fees.service_desc').by_quarter(params[:year].to_i, params[:quarter].to_i).joins(:service_fee).group('service_fees.service_desc').order('COUNT(service_fee_id) DESC').count.first(8)
      else
        services = BookingServiceLine.select('service_fees.service_desc').by_year(params[:year].to_i).joins(:service_fee).group('service_fees.service_desc').order('COUNT(service_fee_id) DESC').count.first(8)
      end
    else
      services = BookingServiceLine.select('service_fees.service_desc').joins(:service_fee).group('service_fees.service_desc').order('COUNT(service_fee_id) DESC').count.first(5)
    end
    services.map { |service, count| increment_hash_by(@hash, service, count ) } 
    respond_to do |format|
      format.html { render }
      format.json { render json: @hash }
    end
  end

  def bookings
    @hash = Hash.new
    bookings = nil

    if params.has_key?(:year)
      bookings = Booking.by_year(params[:year].to_i)  
    else
      bookings = Booking.all
    end

    case params[:scope]
      when "monthly"
        bookings.map { |x| increment_hash(@hash, x.start_date.strftime("%B")) }
      when "yearly"
        bookings.map { |x| increment_hash(@hash, x.start_date.strftime("%Y")) }
      when "quarterly"
        bookings.map { |x|
          month = x.start_date.strftime("%-m").to_i
          if month.between?(1,3)
            increment_hash(@hash, "Quarter 1")
          elsif month.between?(4,6)
            increment_hash(@hash, "Quarter 2")
          elsif month.between?(7,9)
            increment_hash(@hash, "Quarter 3")
          else
            increment_hash(@hash, "Quarter 4")
          end
        }
      else
        bookings.map { |x| increment_hash(@hash, "Overall") }
    end

    respond_to do |format|
      format.html { render }
      format.json { render json: @hash }
    end

  end

  def revenue_by_service
    services = nil
    @hash = Hash.new
    eval_proc = nil

    if params.has_key?(:year)
      if params.has_key?(:month)
        services = BookingServiceLine.by_month(params[:year].to_i, Date::MONTHNAMES.index(params[:month])) 
      elsif params.has_key?(:quarter)
        services = BookingServiceLine.by_quarter(params[:year].to_i, params[:quarter].to_i)
      else
        services = BookingServiceLine.by_year(params[:year].to_i)
      end
    else
      services = BookingServiceLine.all
    end   

    services.map{ |service_line|
      if service_line.service_fee.std_price != 0
        increment_hash_by(@hash, service_line.service_fee, service_line.service_fee.std_price)
      end
    }
    respond_to do |format|
      format.html { render }
      format.json { render json: @hash }
    end
  end


  def evaluate(hash, val, proc)
    if proc.call
      increment_hash(@hash, val)
    end
  end

  def increment_hash(hash, val)
    increment_hash_by(hash, val, 1)
  end

  def increment_hash_by(hash, key, val)
    unless val.nil?
      hash[key] = hash[key].nil? ?  val : hash[key] += val
    end

  end

end
