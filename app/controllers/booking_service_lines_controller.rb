class BookingServiceLinesController < ApplicationController
  before_action :set_booking_service_line, only: [:show, :edit, :update, :destroy]

  # GET /booking_service_lines
  # GET /booking_service_lines.json
  def index
    @booking_service_lines = BookingServiceLine.all
  end

  # GET /booking_service_lines/1
  # GET /booking_service_lines/1.json
  def show
  end

  # GET /booking_service_lines/new
  def new
    @booking_service_line = BookingServiceLine.new
  end

  # GET /booking_service_lines/1/edit
  def edit
  end

  # POST /booking_service_lines
  # POST /booking_service_lines.json
  def create
    @booking_service_line = BookingServiceLine.new(booking_service_line_params)

    respond_to do |format|
      if @booking_service_line.save
        format.html { redirect_to @booking_service_line, notice: 'Booking service line was successfully created.' }
        format.json { render :show, status: :created, location: @booking_service_line }
      else
        format.html { render :new }
        format.json { render json: @booking_service_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /booking_service_lines/1
  # PATCH/PUT /booking_service_lines/1.json
  def update
    respond_to do |format|
      if @booking_service_line.update(booking_service_line_params)
        format.html { redirect_to @booking_service_line, notice: 'Booking service line was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking_service_line }
      else
        format.html { render :edit }
        format.json { render json: @booking_service_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /booking_service_lines/1
  # DELETE /booking_service_lines/1.json
  def destroy
    @booking_service_line.destroy
    respond_to do |format|
      format.html { redirect_to booking_service_lines_url, notice: 'Booking service line was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking_service_line
      @booking_service_line = BookingServiceLine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_service_line_params
      params.require(:booking_service_line).permit(:booking_id, :discount_type_id, :service_fee_id, :bsl_cost, :quantity, :discount_amount, :deleted_at)
    end
end
