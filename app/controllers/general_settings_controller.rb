class GeneralSettingsController < ApplicationController
  def index
  end

  def service_fees
    render "service_fees/index"
  end
end
