class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  def index
    @bookings = Booking.all
    respond_to do |format|
      format.html
      format.json { render json: BookingsDatatable.new(view_context) }
    end
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  # GET /bookings/new
  def new
    @booking = Booking.new
    if params.has_key?(:start_date) && params.has_key?(:end_date)
      @booking.start_date = params[:start_date]
      @booking.end_date = params[:end_date]
    end

    if params.has_key?(:customer_id) && (c = Customer.find(params[:customer_id]))
      @booking.customers << c
    elsif @booking.customers.count == 0
      @booking.customers.build
    end

    1.times { @booking.booking_service_lines.build }

 end

  # GET /bookings/1/edit
  def edit
    @booking = Booking.find(params[:id])
  end

  # POST /bookings
  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)
    customer = Customer.find_or_create_by(customer_params)
    @booking.customers << customer
    respond_to do |format|
      if @booking.save
        format.html { redirect_to root_path, notice: 'Booking was successfully created.' }
        format.json { render :show, status: :created, location: @booking }
      else
        format.html { render :new }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    customer = Customer.find_or_create_by(customer_params)
    @booking.customer_bookings.each do |cb|
      cb.destroy!
    end
    @booking.customers << customer
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to root_path, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Booking was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  def send_booking_reminder
    if (booking = Booking.find(params[:booking_id]))
      send_reminder(booking)
    end
    head :ok
  end

  def send_reminder(booking)
    if (customer = booking.customers.first)
        ApplicationMailer.reminder(customer.id).deliver_now
    end
  end
  
  def send_monthly_reminder
    startdate = Chronic.parse(params[:month])
    enddate = startdate.end_of_month

    if (bookings = Booking.where(start_date: startdate..enddate))
      bookings.each do |booking|
        send_reminder(booking)
      end
    end
    head :ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_booking
    @booking = Booking.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def booking_params
    params.permit(:customer_id)
    params.require(:booking).permit(:discount_type_id, :start_date, :start_time, :end_date, :end_time, :start_time_string, :end_time_string, :bookingdetails, :discountamount, :bookingtotal, booking_service_lines_attributes: [:id, :booking_id, :service_fee_id, :_destroy] )
  end
  def customer_params
    params.require(:booking).require(:customers).permit( :cf_name, :cl_name, :phone1, :email)
  end
end
