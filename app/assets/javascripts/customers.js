$(document).on('turbolinks:load', function() {
    $(window).on('popstate', function() {
        return location.reload(true);
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('table[role=\'datatable\']').each(function() {
        var table;
        var tableID = $(this).attr('id');
        table = $(this).DataTable({
            dom: 'Bfrtip',
            "search": {
                "caseInsensitive": false
            },
            buttons: ['copy',
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
                {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
                {
                    text: 'Custom PDF',
                    extend: 'pdfHtml5',
                    filename: 'dt_custom_pdf',
                    orientation: 'landscape', //portrait
                    pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                    exportOptions: {
                        columns: ':visible',
                        search: 'applied',
                        order: 'applied'
                    },
                    customize: function (doc) {
                        doc.content.splice(0,1);
                        var now = new Date();
                        var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                        doc.pageMargins = [20,60,20,30];
                        doc.defaultStyle.fontSize = 7;
                        doc.styles.tableHeader.fontSize = 7;
                        doc['header']=(function() {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        italics: true,
                                        text: `Report of ${tableID}`,
                                        fontSize: 18,
                                        margin: [10,0]
                                    },
                                    {
                                        alignment: 'right',
                                        fontSize: 14,
                                        text: 'TerriG'
                                    }
                                ],
                                margin: 20
                            }
                        });
                        doc['footer']=(function(page, pages) {
                            return {
                                columns: [
                                    {
                                        alignment: 'left',
                                        text: ['Created on: ', { text: jsDate.toString() }]
                                    },
                                    {
                                        alignment: 'right',
                                        text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                    }
                                ],
                                margin: 20
                            }
                        });
                        // Change dataTable layout (Table styling)
                        // To use predefined layouts uncomment the line below and comment the custom lines below
                        // doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                        var objLayout = {};
                        objLayout['hLineWidth'] = function(i) { return .5; };
                        objLayout['vLineWidth'] = function(i) { return .5; };
                        objLayout['hLineColor'] = function(i) { return '#aaa'; };
                        objLayout['vLineColor'] = function(i) { return '#aaa'; };
                        objLayout['paddingLeft'] = function(i) { return 4; };
                        objLayout['paddingRight'] = function(i) { return 4; };
                        doc.content[0].layout = objLayout;
                    }
                },
            'colvis'
            ],
            processing: true,
            serverSide: true,
            ajax: $(this).data('url')
        });
        return $(this).on('click', 'tr', function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });
    });

    $('#customer tbody').on('click', 'tr', function() {
        var $row, data_set;
        $row = $(this).closest('tr');
        data_set = {
            cust_id: $row.find('td:nth-child(1)').text()
        };
        return $.ajax({
            url: '/customers/get_latest_bookings',
            type: 'post',
            data: data_set,
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            },
            dataType: 'json',
            success: function(d) {
                var t;
                t = $('#customer_booking').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['copy',
                        {
                            extend: 'excel',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            text: 'Custom PDF',
                            extend: 'pdfHtml5',
                            filename: 'dt_custom_pdf',
                            orientation: 'landscape', //portrait
                            pageSize: 'A4', //A3 , A5 , A6 , legal , letter
                            exportOptions: {
                                columns: ':visible',
                                search: 'applied',
                                order: 'applied'
                            },
                            customize: function (doc) {
                                doc.content.splice(0,1);
                                var now = new Date();
                                var jsDate = (now.getMonth()+1)+'-'+now.getDate()+'-'+now.getFullYear();
                                doc.pageMargins = [20,60,20,30];
                                doc.defaultStyle.fontSize = 7;
                                doc.styles.tableHeader.fontSize = 7;
                                doc['header']=(function() {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                italics: true,
                                                text: 'dataTables',
                                                fontSize: 18,
                                                margin: [10,0]
                                            },
                                            {
                                                alignment: 'right',
                                                fontSize: 14,
                                                text: 'Report of '
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                                doc['footer']=(function(page, pages) {
                                    return {
                                        columns: [
                                            {
                                                alignment: 'left',
                                                text: ['Created on: ', { text: jsDate.toString() }]
                                            },
                                            {
                                                alignment: 'right',
                                                text: ['page ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                            }
                                        ],
                                        margin: 20
                                    }
                                });
                                var objLayout = {};
                                objLayout['hLineWidth'] = function(i) { return .5; };
                                objLayout['vLineWidth'] = function(i) { return .5; };
                                objLayout['hLineColor'] = function(i) { return '#aaa'; };
                                objLayout['vLineColor'] = function(i) { return '#aaa'; };
                                objLayout['paddingLeft'] = function(i) { return 4; };
                                objLayout['paddingRight'] = function(i) { return 4; };
                                doc.content[0].layout = objLayout;
                            }
                        },
                    {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        'colvis'
                    ],
                    data: d,
                    columns: [
                        {
                            'data': 'id'
                        }, {
                            'data': 'discount_type_id'
                        }, {
                            'data': 'bookingdetails'
                        }, {
                            'data': 'discountamount'
                        }, {
                            'data': 'bookingtotal'
                        }, {
                            'data': 'start_date'
                        }, {
                            'data': 'start_time'
                        }, {
                            'data': 'end_date'
                        }, {
                            'data': 'end_time'
                        }
                    ]
                });
                return t.destroy();
            }
        });
    });
    var submitButton = document.getElementsByName('customer_create')[0];
    if (submitButton) {
        submitButton.addEventListener('click', function() {

            sanitizePhones();
        });
    };
    $('#customer_booking').on('click', 'tbody tr', function() {
        var data = $(this).closest('tr').find('td:nth-child(1)').text();
        location.href = '/bookings/'+ data +'/edit'
    });
    if ($('meta[name=psj]').attr('controller') === 'customers' && $('meta[name=psj]').attr('action') === 'new') {
        $('#postal_code').attr('maxlength', '5');

        $("#sbtbtn").prop('disabled', true);
        var toValidate = $('#customer_cf_name, #customer_cl_name, #customer_email, #p_numb1'),
            valid = false;
        toValidate.keyup(function () {
            if ($(this).val().length > 0) {
                $(this).data('valid', true);
            } else {
                $(this).data('valid', false);
            }
            toValidate.each(function () {
                if ($(this).data('valid') == true) {
                    valid = true;
                } else {
                    valid = false;
                }
            });
            if (valid === true) {
                $("#sbtbtn").prop('disabled', false);
            } else {
                $("#sbtbtn").prop('disabled', true);
            }
        });
        /***** Prevent user accidentally press enter and submit form ****/
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    }
    /******document.load*******/
});



/******Auto complete address*******/
    // This example displays an address form, using the autocomplete feature
    // of the Google Places API to help users fill in the information.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
        {types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(component).value = '';
        document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
        }
    }
    var street_number = document.getElementById('street_number').value;
    var route = document.getElementById('route').value;
    var state = document.getElementById('administrative_area_level_1').value;
    document.getElementById('customer_address1').value = street_number + ' ' + route;
    $("#col_sel > option").each(function() {
        if(this.text == state){
            this.selected = true;
            return false;
        }
        else return true;
    });
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}



$(document).ready(function() {
    // Function to convert an img URL to data URL
    function getBase64FromImageUrl(url) {
        var img = new Image();
        img.crossOrigin = "anonymous";
        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width =this.width;
            canvas.height =this.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        };
        img.src = url;
    }

});