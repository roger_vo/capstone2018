$(document).on('turbolinks:load', function() {
    $('#calendar').fullCalendar({
        eventColor: '#3C6478',
        eventTextColor: 'white',
        events: '/api/events/all',
        eventLimit: true,
        aspectRatio: 2,
        customButtons: {
            newBooking: {
                text: 'New Booking',
                click: function() {
                    var date = moment();
                    navNewBooking(date);
                }
            },
            sendReminder: {
                text: 'Send Reminders',
                click: function() {
                    if (confirm("Are you sure?")) {
                        sendReminders();
                    }
                }
            }
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,list newBooking sendReminder'
        },
        eventRender: function(event, element) {
            return element.popover({
                title: event.title,
                content: event.description,
                html: true,
                trigger: 'hover',
                placement: 'top',
                container: 'body'
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            return window.location.href = calEvent.edit_callback;
        },
        dayClick: function(date, jsEvent, view) {
            var now = new Date(Date.now());
            var today = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate()));
            /* Checks to see if the date clicked was greater than a day ago */
            if (date.isSameOrAfter(today)) {
                navNewBooking(date);
            }
        },
    });
  if ($('#bd').is(':visible')) {
    $('#bd_calendar').fullCalendar({
      eventColor: 'green',
      eventTextColor: 'white',
      aspectRatio: 2,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek sendReminder'
      },
      customButtons: {
        sendReminder: {
          text: 'Send Reminders',
          click: function() {
            if (confirm("Are you sure?")) {
              sendBdayReminders();
            }
          }
        }
      },
      events: 'api/events/important',
      eventLimit: true,
      eventClick: function(calEvent, jsEvent, view) {
        return window.location.href = calEvent.edit_callback;
      },
      eventRender: function(event, element) {
        return element.popover({
          title: event.title,
          content: event.title + ' turns ' + event.age + ' today',
          trigger: 'hover',
          placement: 'top',
          container: 'body'
        });
      }
    });
  }
  else {


  }

});
window.sendReminders = function () {
  $.ajax({
    url: '/reminder/all',
    method: 'get',
    data: {
      month: $("#calendar").fullCalendar('getDate').format("YYYY-MM-DD")
    },
    success: function() {
    }
  });
};
window.sendBdayReminders = function () {
  $.ajax({
    url: '/reminder/bday',
    method: 'get',
    data: {
      month: $("#calendar").fullCalendar('getDate').format("YYYY-MM-DD")
    },
    success: function() {
    }
  });
};
window.navNewBooking = function(date) {
  return window.location.href = "bookings/new?start_date=" + date.format("Y-M-D") + "&end_date=" + date.format("Y-M-D");
};
