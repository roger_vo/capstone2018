// window.chartLoad = function(e, legendItem) {
//     var index = legendItem.index;
//     var chart = this.chart;
//     var i, ilen, meta;
//
//     for (i = 0, ilen = (chart.data.datasets || []).length; i < ilen; ++i) {
//         meta = chart.getDatasetMeta(i);
//         // toggle visibility of index if exists
//         if (meta.data[index]) {
//             meta.data[index].hidden = !meta.data[index].hidden;
//         }
//     }
//
//     chart.update();
// };
// $(document).on('turbolinks:load', function() {
//     var original = Chart.defaults.global.legend.onClick;
//     Chart.defaults.global.legend.onLoad = function(e, legendItem) {
//          /* do custom stuff here */
//          original.call(this, e, legendItem);
//     };
//
//
//     var lineChartOptions = {
//         responsive: true,
//         legendCallback: function(chart) {
//             console.log(chart);
//             var legendHtml = [];
//             legendHtml.push('<table>');
//             legendHtml.push('<tr>');
//             for (var i=0; i<chart.data.datasets.length; i++) {
//                 legendHtml.push('<td><div class="chart-legend" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></div></td>');
//                 if (chart.data.datasets[i].label) {
//                     legendHtml.push('<td class="chart-legend-label-text" onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')">' + chart.data.datasets[i].label + '</td>');
//                 }
//             }
//             legendHtml.push('</tr>');
//             legendHtml.push('</table>');
//             return legendHtml.join("");
//         },
//         legend: {
//             display: false
//         }
//     };
// });
