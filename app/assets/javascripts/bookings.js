$(document).on('turbolinks:load', function() {1
  // Initializes our time picker 
  $(".timepicker").timepicker({});
  // Only loads on certain pages, ensuring we aren't loading data on every page load.
  // Matches: /bookings/new?start_date=2018-4-5&end_date=2018-4-5
  // Matches: /bookings/8/edit
  if (/\/bookings\/(?:new.*|\d+\/edit)/.exec(window.location.pathname)){
  }



  $("#lookupCustomer").click(function() {
    $.ajax({
      url: "/customers/lookup",
      method: "POST",
      headers: {
        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
      },
      data: {
        phone1: sanitizePhone($("#booking_customers_phone1").val())
      },
      success: function(data, textStatus, jqXHR) {
        if (data.length  == 1) {
          $("#booking_customers_cf_name").val(data[0].cf_name);
          $("#booking_customers_cl_name").val(data[0].cl_name);
          $("#booking_customers_email").val(data[0].email);
          return $("#booking_customers_id").val(data[0].id);
        }
        else if (data.length > 1) {
          $("#customerSelectBody").empty();
          $("#customerSelect").modal("toggle");
          for (var i = 0; i < data.length; i++) {
            var row = `<tr class="selectable-row">` +
                       `<th scope="row">${i}</th>` +
                          `<td data-value="firstname">${data[i].cf_name}</td><td data-value="lastname">${data[i].cl_name}</td>` +
                          `<td data-value="phone">${data[i].phone1}</td><td data-value="email">${data[i].email}</td>` +
                      `</tr>`
            $("#customerSelectBody").append(row);
          }
        }

      }
    });
  });

  $("#customerSelectBody").on('click', '.selectable-row', function() {
    $(this).addClass("table-success").siblings().removeClass("table-success");
  });

  $("#customerSelect").on('click', '.modal-footer button[data-action=\"submit\"]', function() {
    var selectedRow = document.getElementById("customerSelectBody").querySelector(".selectable-row.table-success");
    if (selectedRow != null) {
      var selectedFields = selectedRow.querySelectorAll('td:not([data-value=""])');
      selectedFields.forEach(function(e) {
        var targetQuery = `[data-for=\"${e.dataset.value}\"]`;
        var target = document.querySelector(targetQuery);
        if (target) {
          target.value = e.textContent;
        }
      });
    }
    $("#customerSelect").modal('toggle');
  });

  $(".grid-header-controls a[data-function=\"reminder\"]").click  (function(event) {
    event.preventDefault();
    if (window.confirm("Are you sure?")) {
      $.ajax({
        url: "/reminder/" + getIdFromPath(),
        method: "GET",
        headers: {
          'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        success: function() {
        }
      });
    }
  });

  var observer = new MutationObserver(
    function(){
       $('#service_fees').find('[data-toggle=\"service-selection\"]').trigger('change');
      updateTotal();
    }
  );

  var serviceFees = document.querySelector("#service_fees");

  if (serviceFees != null) {
    observer.observe(serviceFees, { childList: true });
  };


  $("#service_fees").on ('change', '[data-toggle=\"service-selection\"]', function(e) {
    updateLabelFor(this, $('option:selected', this).data("cost"));
    updateSubtotal();
    updateTotal();
  });

  $('#service_fees').find('[data-toggle=\"service-selection\"]').trigger('change');

  var discountInput = document.querySelector("#booking_discountamount");
  if (discountInput != null) {
    discountInput.oninput = function() {
      updateTotal();
    };
  }

  var subtotalInput = document.querySelector("#booking_bookingtotal");
  if (subtotalInput) {
    subtotalInput.oninput = () => { 
      updateTotal();
    }
  }


  var submitButton = document.querySelector(".actions input");
  if (submitButton) {
    submitButton.addEventListener('click', function(e) {
      sanitizePhones();
    });
  };

});


window.getObjectsById = function(objects, id) {
  return objects.filter(function(data) {
    return String(data.id) === String(id);
  });
};

window.updateLabelFor = function(node, value) {
  var priceLabel;
  priceLabel = node.parentNode.nextElementSibling.getElementsByTagName("label")[0];
  return priceLabel.textContent = Number(value).toFixed(2);
};

window.calculateTotal = function() {
  var total = 0;
  var rows = document.querySelectorAll('#service_fees .nested-fields');
  rows.forEach((row) => {
    cost = Number(row.querySelector('select').selectedOptions[0].dataset.cost);
    total += cost;
  });
  return total.toFixed(2);
};

window.updateSubtotal = () => {
  var subtotalInput = document.getElementById('booking_bookingtotal');
  subtotalInput.value = calculateTotal();
}

window.updateTotal = function() {
  var discountInput = document.getElementById('booking_discountamount');
  var subtotalInput = document.getElementById('booking_bookingtotal');
  var bookingTotalInput = document.getElementById('booking_total');

  discount = Number(discountInput.value).toFixed(2);
  subtotal = Number(subtotalInput.value).toFixed(2);

  bookingTotalInput.value = (subtotal - discount).toFixed(2);
};

window.getIdFromPath = function() {
  var pattern = /\/(?:\w)+\/([0-9]+)\/edit/;
  matches = pattern.exec(window.location.pathname);
  if (matches.length > 1) {
    return matches[1];
  }
  return 0
};

