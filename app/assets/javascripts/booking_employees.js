document.addEventListener('turbolinks:load', () => {

  if(/.*\/\d+\/multiadd/.exec(window.location.pathname)) {
    $('[data-action=\"save\"]').click( (event) => {
      event.preventDefault();

      var table = document.getElementById("selectedEmployees");
      var postData = { booking_employee: { save_data: {} } }
      if (table) {
        for(var i = 1; i < table.rows.length; i++) {
          var row = table.rows[i];

          var id = row.querySelector('[name=\"id\"]').textContent;
          var beStart = row.querySelector('[name=\"be_start\"]').value;
          var beEnd = row.querySelector('[name=\"be_end\"]').value;

          postData.booking_employee.save_data[i - 1] = {
            id: id,
            be_start_time: beStart,
            be_end_time: beEnd
          }

        }

        $.ajax({
          url: window.location.pathname + '/save',
          method: 'POST',
          headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
          },
          data: postData,
          success: () => {
            console.log("saved!");
          }
        });
      }
    });

    $("#selectedEmployees").find("[data-type=\"timepicker\"]").timepicker({}); 
  }

  $("#employeeSelection").DataTable({
    processing: true
  });

  $("tbody").on('click', '[data-action=\"add\"]', function(){
    employeeId = $(this).find('[data-type=\"id\"]').text()    
    $(this).addClass('table-success');
    $.ajax({
      url: '/booking_employees/add',
      method: 'POST',
      headers: {
        'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
      },
      data: {
        employee_id: employeeId,
        booking_id: $('#selectedEmployees').data('booking')
      },
      success: () => {
      }

    });
  });

  var observer = new MutationObserver(() => {
    $("#selectedEmployees").find("[data-type=\"timepicker\"]").timepicker({}); 
  });

  var serviceFees = document.querySelector("#selectedEmployees tbody");
  if (serviceFees != null) {
    observer.observe(serviceFees, { childList: true });
  };



});
