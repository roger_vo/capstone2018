document.addEventListener("turbolinks:load", function() {
    $(".grid-reports-selection a.btn").click (function(event) {
        $(".control").addClass("hidden");

        $('.grid-reports-selection a.btn').removeClass('active');
        $(this).addClass('active');

        controls = $(this).data("controls");
        if (controls != null) {
            $("[data-scope~=\"" + controls + "\"]").removeClass("hidden");
            updateData();
        }
        scope = $(this).data("scope");
        if (scope != null) {
            loadNewData(window.location.pathname + ".json", { scope: scope });
        }
        event.preventDefault();
    });

    $(".grid-reports-controls .control").change(function() {
        updateData();
    });

    $('#loadBtn').on('click', function() {
        $('#form').submit(function(e) {
            var chart, data;
            e.preventDefault();
            data = $(this).serialize();
            chart = Chartkick.charts['booking_chart'];
            chart.updateData(chart.getDataSource() + '?' + data);
        });
    });
});

window.updateData= function() {
  var params = new Object();

  controls = document.querySelectorAll(".grid-reports-controls select.control:not(.hidden)");

  scope = document.querySelector('.grid-reports-selection a.btn.active').dataset.scope;
  params['scope'] = scope;

  controls.forEach((control) => {
    params[control.name] = control.value
  });

  url = window.location.pathname + ".json";
  loadNewData(url, params);
};

window.loadNewData = function(url, postData) {
  $.ajax({
    url: url,
    method: 'POST',
    headers: {
          'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
    },
    data:  postData,
    success: function(data, textStatus, jqXHR) {
      var chart = Chartkick.charts["chart-1"];
      chart.updateData(data);
    }
  });
};

