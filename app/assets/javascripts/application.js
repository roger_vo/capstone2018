// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require turbolinks
//= require jquery
//= require datatables
//= require jquery-ui
//= require toastr
//= require moment
//= require popper
//= require bootstrap
//= require bootstrap-toggle
//= require fullcalendar
//= require jquery.timepicker.min.js
//= require datepair.min.js
//= require jquery.datepair.min.js
//= require cocoon
//= require Chart.bundle
//= require chartkick
//= require_tree .
document.addEventListener("turbolinks:load", function() {
    $('input[type="checkbox"][data-toggle="toggle"]').bootstrapToggle();
    $('#bd').hide();


    $('#toggle-event').change( function () {
        $('#bk, #bd').toggle()
    });
  addPhoneSanitizers();

  var start = Date.now();
  var lastInputReceived = 0;
  var keyInputs = 0;
  var inputs = '';
  var konamiCode = 'ArrowUpArrowUpArrowDownArrowDownArrowLeftArrowRightArrowLeftArrowRightba'
  document.body.addEventListener('keypress', (event) => {
    var now = Date.now();
    var timeSinceLastInput = (now - start) - lastInputReceived; 

    if(lastInputReceived == 0 || timeSinceLastInput < 1500) {
      inputs += event.key;
      if(inputs == konamiCode) {
        window.location.href = '/supersecret';
      }
    }
    else {
      inputs = '';
    }

    lastInputReceived = (now - start);
  });
});
window.addPhoneSanitizers = function() {
  elements = document.querySelectorAll("[data-type=\"phone\"]");
  elements.forEach(function(element) {
    return element.addEventListener('input', function(e) {
      var x = e.target.value.replace(/\D/g, '').match(/(\d{1,3})(\d{0,3})(\d{0,4})/);
      return e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
  });
};

window.sanitizePhones = function() {
  var phoneInputs = document.querySelectorAll("[data-type=\"phone\"]");
  phoneInputs.forEach(function(e) {
    e.value = sanitizePhone(e.value);
  });
};

window.sanitizePhone = function(phone) {
  var match = phone.match(/\((\d{3})\)\ (\d{3})-(\d{4})/);
  return match[3] ? match[1] + match[2] + match[3] : '';
};



