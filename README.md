## [Demo](https://lit-wildwood-88832.herokuapp.com/)

Test user login:
- ID: test@test.com
- Password: test123
### Project Description
    
This project began on January 24th and concluded on April 18th, when GBMX Consulting finished developing an application 
that met the owner’s, TerriG, user, and functional requirements. After planning and conducting an
analysis of the business, GBMX Consulting proposed four high-level objectives for the project to
which TerriG agreed upon. One of the team’s objectives for the project was the ability to
schedule photo-shoot services. This allowed TerriG Photography to have better control over the
bookings that are scheduled throughout the year and ensured that bookings cannot be booked at
the same time (known as a double booking). Maintaining customer information is another high-
level objective that allowed TerriG Photography to send out confirmations and reminders as well
as assist in expanding her customer network by establishing proper customer management. In
addition, the application was designed to modify the services provided to reflect the changes in
price and the seasonal offers that she promotes on her website. Lastly, the application allowed
the running of analytical reports to show TerriG Photography data and trends related to the
business.
