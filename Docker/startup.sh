#!/bin/bash

echo Creating database
runuser -l app -c "cd ${APP_PATH} && rake db:create RAILS_ENV=production"
echo Migrating database
runuser -l app -c "cd ${APP_PATH} && rake db:migrate RAILS_ENV=production"
echo Compiling Assets
runuser -l app -c "cd ${APP_PATH} && rake assets:precompile RAILS_ENV=production"
