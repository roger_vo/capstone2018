require 'test_helper'

class BookingEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @booking_employee = booking_employees(:one)
  end

  test "should get index" do
    get booking_employees_url
    assert_response :success
  end

  test "should get new" do
    get new_booking_employee_url
    assert_response :success
  end

  test "should create booking_employee" do
    assert_difference('BookingEmployee.count') do
      post booking_employees_url, params: { booking_employee: { be_end_time: @booking_employee.be_end_time, be_start_time: @booking_employee.be_start_time, booking_id: @booking_employee.booking_id, deleted_at: @booking_employee.deleted_at, employee_id: @booking_employee.employee_id } }
    end

    assert_redirected_to booking_employee_url(BookingEmployee.last)
  end

  test "should show booking_employee" do
    get booking_employee_url(@booking_employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_booking_employee_url(@booking_employee)
    assert_response :success
  end

  test "should update booking_employee" do
    patch booking_employee_url(@booking_employee), params: { booking_employee: { be_end_time: @booking_employee.be_end_time, be_start_time: @booking_employee.be_start_time, booking_id: @booking_employee.booking_id, deleted_at: @booking_employee.deleted_at, employee_id: @booking_employee.employee_id } }
    assert_redirected_to booking_employee_url(@booking_employee)
  end

  test "should destroy booking_employee" do
    assert_difference('BookingEmployee.count', -1) do
      delete booking_employee_url(@booking_employee)
    end

    assert_redirected_to booking_employees_url
  end
end
