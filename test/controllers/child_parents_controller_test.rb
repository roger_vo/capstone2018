require 'test_helper'

class ChildParentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @child_parent = child_parents(:one)
  end

  test "should get index" do
    get child_parents_url
    assert_response :success
  end

  test "should get new" do
    get new_child_parent_url
    assert_response :success
  end

  test "should create child_parent" do
    assert_difference('ChildParent.count') do
      post child_parents_url, params: { child_parent: { child_id: @child_parent.child_id, customer_id: @child_parent.customer_id, deleted_at: @child_parent.deleted_at } }
    end

    assert_redirected_to child_parent_url(ChildParent.last)
  end

  test "should show child_parent" do
    get child_parent_url(@child_parent)
    assert_response :success
  end

  test "should get edit" do
    get edit_child_parent_url(@child_parent)
    assert_response :success
  end

  test "should update child_parent" do
    patch child_parent_url(@child_parent), params: { child_parent: { child_id: @child_parent.child_id, customer_id: @child_parent.customer_id, deleted_at: @child_parent.deleted_at } }
    assert_redirected_to child_parent_url(@child_parent)
  end

  test "should destroy child_parent" do
    assert_difference('ChildParent.count', -1) do
      delete child_parent_url(@child_parent)
    end

    assert_redirected_to child_parents_url
  end
end
