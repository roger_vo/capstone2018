require 'test_helper'

class BookingServiceLinesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @booking_service_line = booking_service_lines(:one)
  end

  test "should get index" do
    get booking_service_lines_url
    assert_response :success
  end

  test "should get new" do
    get new_booking_service_line_url
    assert_response :success
  end

  test "should create booking_service_line" do
    assert_difference('BookingServiceLine.count') do
      post booking_service_lines_url, params: { booking_service_line: { booking_id: @booking_service_line.booking_id, bsl_cost: @booking_service_line.bsl_cost, deleted_at: @booking_service_line.deleted_at, discount_amount: @booking_service_line.discount_amount, discount_type_id: @booking_service_line.discount_type_id, quantity: @booking_service_line.quantity, service_fee_id: @booking_service_line.service_fee_id } }
    end

    assert_redirected_to booking_service_line_url(BookingServiceLine.last)
  end

  test "should show booking_service_line" do
    get booking_service_line_url(@booking_service_line)
    assert_response :success
  end

  test "should get edit" do
    get edit_booking_service_line_url(@booking_service_line)
    assert_response :success
  end

  test "should update booking_service_line" do
    patch booking_service_line_url(@booking_service_line), params: { booking_service_line: { booking_id: @booking_service_line.booking_id, bsl_cost: @booking_service_line.bsl_cost, deleted_at: @booking_service_line.deleted_at, discount_amount: @booking_service_line.discount_amount, discount_type_id: @booking_service_line.discount_type_id, quantity: @booking_service_line.quantity, service_fee_id: @booking_service_line.service_fee_id } }
    assert_redirected_to booking_service_line_url(@booking_service_line)
  end

  test "should destroy booking_service_line" do
    assert_difference('BookingServiceLine.count', -1) do
      delete booking_service_line_url(@booking_service_line)
    end

    assert_redirected_to booking_service_lines_url
  end
end
