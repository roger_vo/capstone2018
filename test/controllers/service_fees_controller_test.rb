require 'test_helper'

class ServiceFeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @service_fee = service_fees(:one)
  end

  test "should get index" do
    get service_fees_url
    assert_response :success
  end

  test "should get new" do
    get new_service_fee_url
    assert_response :success
  end

  test "should create service_fee" do
    assert_difference('ServiceFee.count') do
      post service_fees_url, params: { service_fee: { deleted_at: @service_fee.deleted_at, service_desc: @service_fee.service_desc, service_type: @service_fee.service_type, std_price: @service_fee.std_price } }
    end

    assert_redirected_to service_fee_url(ServiceFee.last)
  end

  test "should show service_fee" do
    get service_fee_url(@service_fee)
    assert_response :success
  end

  test "should get edit" do
    get edit_service_fee_url(@service_fee)
    assert_response :success
  end

  test "should update service_fee" do
    patch service_fee_url(@service_fee), params: { service_fee: { deleted_at: @service_fee.deleted_at, service_desc: @service_fee.service_desc, service_type: @service_fee.service_type, std_price: @service_fee.std_price } }
    assert_redirected_to service_fee_url(@service_fee)
  end

  test "should destroy service_fee" do
    assert_difference('ServiceFee.count', -1) do
      delete service_fee_url(@service_fee)
    end

    assert_redirected_to service_fees_url
  end
end
