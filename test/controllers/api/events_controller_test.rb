require 'test_helper'

class Api::EventsControllerTest < ActionDispatch::IntegrationTest
  test "should get all" do
    get api_events_all_url
    assert_response :success
  end

  test "should get important" do
    get api_events_important_url
    assert_response :success
  end

end
