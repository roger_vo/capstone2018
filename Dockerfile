FROM phusion/passenger-full:0.9.29

# env vars
ENV HOME /root
ENV APP_HOME /home/app
ENV APP_PATH $APP_HOME/webapp
ENV START_SCRIPT_DIR /etc/my_init.d

# something passenger needs
CMD ["/sbin/my_init"]

RUN bash -lc 'rvm --default use 2.4.3'

RUN gem install bundler
RUN gem install rails --no-rdoc --no-ri

# pull our code and fix permissions
# RUN git clone https://github.com/adrubesh/rails-booking-calendar.git $APP_PATH -b staging
ADD . $APP_PATH
RUN chown -R app:app $APP_PATH 

# run bundle install as the app user
RUN runuser -l app -c "bundle install --gemfile=${APP_PATH}/Gemfile --without=Development Test"

# add our database.yml/secrets.yml file and setup the database
ADD [--chown=app:app] Docker/database.yml $APP_PATH/config/database.yml
ADD [--chown=app:app] Docker/secrets.yml $APP_PATH/config/secrets.yml
ADD [--chown=app:app] Docker/startup.sh $START_SCRIPT_DIR/setup.sh
RUN chmod +x $START_SCRIPT_DIR/setup.sh
RUN chown app:app $APP_PATH/config/secrets.yml
RUN echo $APP_PATH/config/secrets.yml

# pull our webapp.conf
# RUN git clone https://github.com/adrubesh/docker-tgcal-nginxconf.git /home/app/webapp-conf
ADD Docker/nginx.conf /etc/nginx/sites-enabled/webapp.conf
# RUN cp /home/app/webapp-conf/nginx.conf /etc/nginx/sites-enabled/webapp.conf

# webservers run on port 80! :-)
EXPOSE 80

# delete nginx default config
RUN rm -f /etc/nginx/sites-enabled/default

# start nginx 
RUN rm -f /etc/service/nginx/down



# cleanup tasks
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
