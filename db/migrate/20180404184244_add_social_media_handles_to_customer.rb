class AddSocialMediaHandlesToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :facebook_handle, :string
    add_column :customers, :instagram_handle, :string
    add_column :customers, :twitter_handle, :string
    add_column :customers, :customer_type, :boolean
  end
end
