class AddDeletedAtToCustomers < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :deleted_at, :timestamp
  end
end
