# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
require 'csv'

def import_csv(file, model)
  truncate_table(model)
  path = File.join("lib","seeds", file)
  text = File.read(path)
  unless text.empty?
    csv = CSV.parse(text, headers: true)
    csv.each do |row|
       model.create!(row.to_h)
    end
    puts "Loaded: #{model} ... #{if csv.count == model.all.count then "\e[32msuccess\e[0m" else "\e[31mfailed\e[0m" end }"
    reset_sequences(model)
  end
end
def reset_sequences(model)
  adapter = ActiveRecord::Base.connection.adapter_name.downcase.to_sym
  case adapter
  when :postgresql
   result = ActiveRecord::Base.connection.execute("SELECT setval('#{model.table_name}_id_seq', #{model.count}, true);")
   puts result 
  end
end
def truncate_table(model)
  adapter = ActiveRecord::Base.connection.adapter_name.downcase.to_sym
  case adapter
  when :postgresql
    truncate_postgres(model)
  when :sqlite
    truncate_sqlite(model)
  else 
    puts "Could not truncate #{model}. Unknown database adapter"
  end
end
def truncate_sqlite(model)
  ActiveRecord::Base.connection.execute("DELETE FROM #{model.table_name}")
  ActiveRecord::Base.connection.execute("DELETE FROM SQLITE_SEQUENCE WHERE name='#{model.table_name}'")
  reset_seq
end

def truncate_postgres(model)
  ActiveRecord::Base.connection.execute("TRUNCATE TABLE #{model.table_name} RESTART IDENTITY;")
  reset_seq
end
def reset_seq
  ActiveRecord::Base.connection.tables.each do |t|
    ActiveRecord::Base.connection.reset_pk_sequence!(t)
  end
end
if Rails.env.development? or Rails.env.staging?
    User.create([
      { email: "test@test.com", password: "test123"},
      { email: "terrig@tgcal.com", password: "gbmx123" }
    ])
    import_csv("customers.csv", Customer)
    import_csv("states.csv", State)
    import_csv("service_fees.csv", ServiceFee)
    import_csv("discount_types.csv", DiscountType)
    import_csv("bookings.csv", Booking)
    import_csv("children.csv", Child)
    import_csv("employee_types.csv", EmployeeType)
    import_csv("employees.csv", Employee)
    import_csv("customer_bookings.csv", CustomerBooking)
    import_csv("booking_service_lines.csv", BookingServiceLine)
    import_csv("booking_employees.csv", BookingEmployee)
    import_csv("child_parents.csv", ChildParent)
end


#if Rails.env.production?
#  users = User.create(email: "admin@tgcal.com", password: "admin")
#end
