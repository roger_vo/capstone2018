# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180411130735) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "booking_employees", force: :cascade do |t|
    t.integer "employee_id"
    t.integer "booking_id"
    t.time "be_start_time"
    t.time "be_end_time"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_booking_employees_on_booking_id"
    t.index ["employee_id"], name: "index_booking_employees_on_employee_id"
  end

  create_table "booking_service_lines", force: :cascade do |t|
    t.integer "booking_id"
    t.integer "service_fee_id"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_booking_service_lines_on_booking_id"
    t.index ["service_fee_id"], name: "index_booking_service_lines_on_service_fee_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.integer "discount_type_id"
    t.string "bookingdetails"
    t.decimal "discountamount", default: "0.0"
    t.decimal "bookingtotal", default: "0.0"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "start_date"
    t.time "start_time"
    t.date "end_date"
    t.time "end_time"
    t.index ["discount_type_id"], name: "index_bookings_on_discount_type_id"
  end

  create_table "child_parents", force: :cascade do |t|
    t.integer "child_id"
    t.integer "customer_id"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["child_id"], name: "index_child_parents_on_child_id"
    t.index ["customer_id"], name: "index_child_parents_on_customer_id"
  end

  create_table "children", force: :cascade do |t|
    t.string "c_fName"
    t.string "c_lName"
    t.date "c_birthday"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customer_bookings", force: :cascade do |t|
    t.integer "booking_id"
    t.integer "customer_id"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_id"], name: "index_customer_bookings_on_booking_id"
    t.index ["customer_id"], name: "index_customer_bookings_on_customer_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "cf_name"
    t.string "cl_name"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.integer "state_id"
    t.string "zipcode"
    t.date "birthday"
    t.string "phone1"
    t.string "phone2"
    t.string "email"
    t.string "facebook_id"
    t.string "twitter_id"
    t.string "instagram_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facebook_handle"
    t.string "instagram_handle"
    t.string "twitter_handle"
    t.boolean "customer_type"
    t.datetime "deleted_at"
    t.index ["state_id"], name: "index_customers_on_state_id"
  end

  create_table "discount_types", force: :cascade do |t|
    t.string "discount_type"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_types", force: :cascade do |t|
    t.string "employee_type"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employees", force: :cascade do |t|
    t.integer "employee_type_id"
    t.string "ef_name"
    t.string "el_name"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["employee_type_id"], name: "index_employees_on_employee_type_id"
  end

  create_table "service_fees", force: :cascade do |t|
    t.decimal "std_price", precision: 8, scale: 2
    t.text "service_desc"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", force: :cascade do |t|
    t.string "state_name"
    t.string "state_abbvr"
    t.date "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
