namespace :custom do
  desc 'seed admin user to db'
  task :seed_admin, :param do
    on roles(:app, :db) do 
      within "#{current_path}" do
      with rails_env: "staging" do
        execute :rake, "db:seed"
      end
      end
    end
  end

  desc 'reset database'
  task :reset_db, :param do
    on roles(:app, :db) do
      within "#{current_path}" do
        with rails_env: "staging" do
          execute :rake, "db:reset"
        end
      end
    end
  end 
  
  desc 'seed admin user to db'
  task :seed_admin, :param do
    on roles(:app, :db) do 
      within "#{current_path}" do
      with rails_env: "production" do
        execute :rake, "db:seed"
      end
      end
    end
  end


end
